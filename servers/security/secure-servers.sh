#!/bin/bash

public_key=ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQChjEYPG4gfq4v4ZDzH4T0aZSKnQqTv3dhHhtsuUXUdW8JdZZsKABSrb55qsOjYPB4hw2hIuYROU93tDlZZfg51Bh4FUaoy68k+5gCfIe7KOdx4OhzLW5t0tJTImT5T91zibxy43gmT2b46TW2rBfqH2bwwzPitDUbv8Mfz5lEWC8VvrljZ4EmhzI+AdNkgFW1OINzT/DOUvcTckEpsVY4Qdj1gCg5guOSEtWRjHpirmqVIhpKPQIxZMszut2Xg4BR2AGWs5OG4AXpaOrf8d5rCe8Tovj4WxFhITcIPTKRTMjphkUuSCZRAXcSXMTZLDS5emB4EjPSvzai7dpYr4iqB09NC+woTXVVR9WV0vP2MmQvG6Vgj7K3fZNPZJDR+WQlvQqka8+2jvUKzVKZ1h56F9ogtHqd1Epq0djiLzr/XyPxUObqV1VjIgPu/LLOGtqOfbe01gfQ0Ag/6VVL99PB1mSUwknMeLQOIFm7f77e8JQWhBJmzbr8fZa4Opc7QAZ/6Rf/sGun2AAXLgCCK8NrKz39pxsRkeMAND3pacV4D06LB6emdCtxsydeWhvGCcvFRQ9FZjSGTILCl4S5YCOydnBzoMbdZbWzlTp0euUw8RnZBU/xtxyIKzdSRyq0MAByPS6iYAlo3oAmI/sgr1ffvQ6sVxoBg7ZNaGxNsB9G77Q== gonzalo@moodaway.com

# Install Firewall
apt-get install --yes ufw

   # Set Rules
ufw default deny incoming
ufw default allow outgoing

ufw allow ssh
ufw allow 22/tcp
ufw allow 2222/tcp
ufw allow www
sudo ufw allow 80/tcp

   # Activate
ufw enable
ufw status

   # Disable password authentication
if grep -Fxq "$public_key" ~./ssh/authorized_keys
then
    echo "Key already authorized"
else
    echo "$public_key" >> ~/.ssh/authorized_keys
    echo "Key authorized"
fi

sed -i 's/ChallengeResponseAuthentication yes/ChallengeResponseAuthentication no/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication no/PasswordAuthentication no/g' /etc/ssh/sshd_config
sed -i 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config

