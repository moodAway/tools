#!/bin/bash

   # Install and configure Jenkins
wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update
apt-get install -y jenkins
mkdir /var/lib/jenkins/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTOcVyWQwzOWRwUQIXo2F7kLrF0Z0di4lXlL3IV6Keaz1dbsAQVIIxi3XlSR+ShRw05BAvs3E5H6ZNu8VNegwEBTIkYEbIsxCPECRwSCTwQ5BsyhUTW5hUZZGF99z7eguV9TTe8F33WF5akAuYdbk8ANGvSR4G1K2wJXvThseCQ0153gpr3qNNqxkcf/o9htFK6bOX/8I2hTFkIwDKLxmQPz48k+DcSVojcCLSvkQSTky6GCPIj4Gc0SgYmWzSSf7zEw+grQErhD5uNukzJ7edop61nci7CZlRbEsNHOVYWAHuHXps1cCtkpgMcYM+JmUVVW+rUhN0CrOIUIlHyj7p jenkins@jenkins.moodaway.com" >> /var/lib/jenkins/.ssh/authorized_keys

   # Install Bower Globally
npm install -g bower

   # Install Gulp Globally
npm install --global gulp

   # Install Apache
apt-get install -y apache2
a2enmod rewrite
chown -R jenkins:jenkins /var/www/html/

   # Set Time Zone
echo "Europe/London" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata
