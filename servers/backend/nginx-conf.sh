#!/bin/bash

   # Production
mkdir /home/dokku/api/nginx.conf.d
echo "client_max_body_size 50M;" >> /home/dokku/api/nginx.conf.d/upload.conf
echo "more_set_headers 'Access-Control-Allow-Origin: *;'" >> /home/dokku/api/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, HEAD;'" >> /home/dokku/api/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Credentials: true;'" >> /home/dokku/api/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization;'" >> /home/dokku/api/nginx.conf.d/cors.conf
chown -R dokku:dokku /home/dokku/api/nginx.conf.d

   # Staging
mkdir /home/dokku/stage/nginx.conf.d
echo "client_max_body_size 50M;" >> /home/dokku/stage/nginx.conf.d/upload.conf
echo "more_set_headers 'Access-Control-Allow-Origin: *;'" >> /home/dokku/stage/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, HEAD;'" >> /home/dokku/stage/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Credentials: true;'" >> /home/dokku/stage/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization;'" >> /home/dokku/stage/nginx.conf.d/cors.conf
chown -R dokku:dokku /home/dokku/stage/nginx.conf.d

   # Development
mkdir /home/dokku/dev/nginx.conf.d
echo "client_max_body_size 50M;" >> /home/dokku/dev/nginx.conf.d/upload.conf
echo "more_set_headers 'Access-Control-Allow-Origin: *;'" >> /home/dokku/dev/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, HEAD;'" >> /home/dokku/dev/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Credentials: true;'" >> /home/dokku/dev/nginx.conf.d/cors.conf
echo "more_set_headers 'Access-Control-Allow-Headers: Origin,Content-Type,Accept,Authorization;'" >> /home/dokku/dev/nginx.conf.d/cors.conf
chown -R dokku:dokku /home/dokku/stage/nginx.conf.d

