#!/bin/bash

   # Create Swap file
apt-get dist-upgrade
apt-get install --yes nginx-extras
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile         none            swap    sw                0       0" >> /etc/fstab
swapon -a

   # Install dokku plugins
   # Mongodb
echo "=== Installing dokku plugins, this could take several minutes, please wait"
git clone https://github.com/jeffutter/dokku-mongodb-plugin.git /var/lib/dokku/plugins/mongodb
dokku plugins-install
dokku mongodb:start

   # Shoreman
git clone https://github.com/statianzo/dokku-shoreman.git /var/lib/dokku/plugins/dokku-shoreman
dokku plugins-install

   # Git revision
git clone https://github.com/cjblomqvist/dokku-git-rev /var/lib/dokku/plugins/dokku-git-rev
dokku plugins-install

   # Volumes
git clone https://github.com/ohardy/dokku-volume /var/lib/dokku/plugins/volume
dokku plugins-install

   # Set timezone
echo "Europe/London" > /etc/timezone
# dpkg-reconfigure -f noninteractive tzdata
