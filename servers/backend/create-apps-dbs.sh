#!/bin/bash
   # Production
echo "=== Creating app and database for production"
dokku apps:create api
dokku mongodb:create api api
dokku mongodb:link api api

   # Staging
echo "=== Creating app and database for staging"
dokku apps:create stage
dokku mongodb:create stage
dokku mongodb:link stage stage

   # Development
echo "=== Createing app and database for development"
dokku apps:create dev
dokku mongodb:create dev dev
dokku mongodb:link dev dev

echo "You have successfully created 3 apps [api,stage,dev]"
