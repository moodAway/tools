# Description

The repository contains a set of scritps to automate the configuration of
Servers

## USAGE

Servers should have Git preinstalled.
Clone this project and run scripts

## Front End Servers

####  Digital Ocean Droplet configuration:
* 1GB Ram
* 30GB SSD Disk
* Server: London 1
* Type: Ubuntu node v4.1.0 on 14.04

### Configuring the server
Run __pre-config.sh__ to install all dependencies required by the system.
After running this script please add the following sections to :
__/etc/apache2/sites-available/000-default.conf__

```
<Directory /var/www/html>
        RewriteEngine on

         # Don't rewrite files or directories
         RewriteCond %{REQUEST_FILENAME} -f [OR]
         RewriteCond %{REQUEST_FILENAME} -d
         RewriteRule ^ - [L]

         # Rewrite everything else to index.html to allow html5 state links
         RewriteRule ^ index.html [L]
</Directory>
```

and restart Apache: ```service apache2 restart```

## Back End Servers

### Digital Ocean Droplet Configuration:
* 1GB Ram
* 30GB SSD Disk
* Server: London 1
* Type: Ubuntu Dokku v0.3.19 on 14.04

### Configuring the server

Run __pre-config.sh__ to install all dependecies required by the system.

Run __create-apps-dbs.sh__ to create apps and databases: This script will
create 3 apps/database (Production, Staging, Development).

Run __set-variables.sh__ to set variables such as Facebook Token, Amazon S3
link to each environment. Parameters: api, stage, dev.

Run __nginx-conf.sh__ to configure nginx for all apps.

