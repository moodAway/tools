#!/bin/bash

STAGE_DB="stage-production"
PROD_DB="api-production"
S3_BUCKET_NAME="moodAway-zeus" #replace with your bucket name on Amazon S3
S3_BUCKET_PATH="mongodb-backups"

# Create backup
dokku mongodb:dump $STAGE_DB -tar
dokku mongodb:dump $PROD_DB -tar

# Upload to S3
s3cmd put *.tar.gz s3://$S3_BUCKET_NAME/$S3_BUCKET_PATH/

# Remove Dumps
rm ./*.tar.gz

