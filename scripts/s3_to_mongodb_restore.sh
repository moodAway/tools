#!/bin/bash

DB=$1
DUMP=$2
S3_BUCKET_NAME="moodAway-zeus" #replace with your bucket name on Amazon S3
S3_BUCKET_PATH="mongodb-backups"

# Download from S3
s3cmd get s3://$S3_BUCKET_NAME/$S3_BUCKET_PATH/$DUMP .

# Restore Dump
dokku mongodb:restore $DB $DUMP

# Remove Dumps
rm ./*.tar.gz
